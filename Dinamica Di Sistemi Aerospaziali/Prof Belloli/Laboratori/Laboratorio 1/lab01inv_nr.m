% Copyright (C) 2009
% 
% Pierangelo Masarati <masarati@aero.polimi.it>
% 
% Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
% via La Masa, 34 - 20156 Milano, Italy
% http://www.aero.polimi.it/
% 
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation (version 2 of the License).
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

% Dinamica di Sistemi Aerospaziali
% Laboratorio informatico 1

% Cinematica inversa

function out = lab01inv_nr(theta, jac)

    lab01_data;

    if (nargin < 2),
        jac = 0;
    end

    if (nargin < 1),
        error('need at least theta');
    end

    if (jac == 0),
        
%         out = ...

    else
%         matrice jacobiana...
%         out = ... [J]
    end
    
end
