% Copyright (C) 2009
% 
% Pierangelo Masarati <masarati@aero.polimi.it>
% 
% Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
% via La Masa, 34 - 20156 Milano, Italy
% http://www.aero.polimi.it/
% 
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation (version 2 of the License).
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

% Dinamica di Sistemi Aerospaziali
% Laboratorio informatico 1

clear all
close all
clc

% Cinematica diretta, dinamica inversa e verifica diretta

global t1 C1 C2

% carico i dati, comuni a tutti i problemi
lab01_data;

% costruisco legge oraria giunto 1
n1 = 20;
o1 = ones(n1, 1);
oh1 = o1/n1;
n2 = 8*20;
o2 = ones(n2, 1);
oh2 = o2/n2;

T1 = 1;
T2 = 8;

a = 1/9*pi/2;
Dt = [oh1*T1; oh2*T2; oh1*T1];
t_thetappm1 = [o1*a; o2*0; -o1*a];
t_thetapp1 = ([0; t_thetappm1] + [t_thetappm1; 0])/2;

t1 = 0.;
t_thetap1 = 0.;
t_theta1 = 0.;
for i = 1:length(Dt),
	t1(1 + i, 1) = t1(i, 1) + Dt(i);
	t_thetap1(1 + i, 1) = t_thetappm1(i)*Dt(i) + t_thetap1(i, 1);
	t_theta1(1 + i, 1) = t_thetappm1(i)/2.*Dt(i)^2 + t_thetap1(i, 1)*Dt(i) + t_theta1(i, 1);
end

% costruisco legge oraria giunto 2
n1 = 10;
o1 = ones(n1, 1);
oh1 = o1/n1;
n2 = 8*20;
o2 = ones(n2, 1);
oh2 = o2/n2;

T1 = .5;
T2 = 8;

a = 4*pi/2;
Dt = [oh1*T1; oh1*T1; oh2*T2; oh1*T1; oh1*T1];
t_thetappm2 = [-o1*a; o1*a; o2*0; o1*a; -o1*a];
t_thetapp2 = ([0; t_thetappm2] + [t_thetappm2; 0])/2;

t2 = 0.;
t_thetap2 = 0.;
t_theta2 = 0.;
for i = 1:length(Dt),
	t2(1 + i, 1) = t2(i, 1) + Dt(i);
	t_thetap2(1 + i, 1) = t_thetappm2(i)*Dt(i) + t_thetap2(i, 1);
	t_theta2(1 + i, 1) = t_thetappm2(i)/2.*Dt(i)^2 + t_thetap2(i, 1)*Dt(i) + t_theta2(i, 1);
end

if (norm(t1 - t2) ~= 0),
	error('vettori tempi non coincidenti');
end

theta1 = t_theta1;
thetap1 = t_thetap1;
thetapp1 = t_thetapp1;
theta2 = t_theta1 + t_theta2;
thetap2 = t_thetap1 + t_thetap2;
thetapp2 = t_thetapp1 + t_thetapp2;

figure;
plot(t1, t_theta1*180/pi, t1, t_theta2*180/pi);
legend('\theta_1', '\theta_2');
xlabel('Time, s');
ylabel('Joint rotation, deg');
title('Computed joint rotation');

figure;
plot(t1, t_thetap1*180/pi, t1, t_thetap2*180/pi);
legend('\theta_1''', '\theta_2''');
xlabel('Time, s');
ylabel('Joint angular velocity, deg/sec');
title('Computed joint angular velocity');

figure;
plot(t1, t_thetapp1*180/pi, t1, t_thetapp2*180/pi);
legend('\theta_1''''', '\theta_2''''');
xlabel('Time, s');
ylabel('Joint angular acceleration, deg/sec^2');
title('Computed joint angular acceleration');

% cinematica diretta: posizione dell'estremo libero in funzione di
% t_theta1, t_theta2

% x = ...
% y = ...

% xp = ...
% yp = ...
% 
% xpp = ... 	
% ypp = ...
	
figure;
plot(t1, x, t1, y);
legend('x', 'y');
xlabel('Time, s');
ylabel('Displacement, m');
title('Imposed displacement');

figure
plot(x, y)
xlabel('x, m');
ylabel('y, m');
title('Imposed displacement');
axis equal

figure;
plot(t1, xp, t1, yp);
legend('x''', 'y''');
xlabel('Time, s');
ylabel('Velocity, m/s');
title('Imposed velocities');

figure;
plot(t1, xpp, t1, ypp);
legend('x''''', 'y''''');
xlabel('Time, s');
ylabel('Acceleration, m/s^2');
title('Imposed accelerations');

% dinamica inversa
% C2 = ...
% C1 = C2 + ...

figure;
plot(t1, C1, t1, C2);
legend('C_1', 'C_2');
xlabel('Time, s');
ylabel('Joint torque, Nm');
title('Computed joint torque');

% dinamica diretta (verifica)
opt = odeset('RelTol', 1e-6, ...
        'AbsTol', 1e-6, ...
        'InitialStep', 0.01, ...
        'MaxStep', 0.01);
X0 = [theta1(1), theta2(1), thetap1(1), thetap2(1)];
T = [0, 10];
[td, X] = ode45(@lab01dir_func, T, X0, opt);

figure;
plot(t1, t_theta1*180/pi, t1, t_theta2*180/pi, td, X(:, 1)*180/pi, td, (X(:, 2)-X(:, 1))*180/pi);
legend('\theta_1 computed', '\theta_2 computed', '\theta_1 verified', '\theta_2 verified');
xlabel('Time, s');
ylabel('Joint rotation, deg');
title('Computed and verified joint rotation');

