function y = cinfun(x, c)
    global a b d
    
    y(1) = a*cos(x(1)) + b*cos(x(2)) + c;
    y(2) = a*sin(x(1)) + b*sin(x(2)) + d;
    
end