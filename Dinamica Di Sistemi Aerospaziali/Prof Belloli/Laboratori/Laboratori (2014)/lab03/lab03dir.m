% Copyright (C) 2009
% 
% Pierangelo Masarati <masarati@aero.polimi.it>
% 
% Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
% via La Masa, 34 - 20156 Milano, Italy
% http://www.aero.polimi.it/
% 
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation (version 2 of the License).
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

clear all
close all
clc

% Dinamica di Sistemi Aerospaziali
% Laboratorio informatico 3

disp(sprintf('\ndati:'))

% massa supposta unitaria
m = 1;

% lunghezza di ogni tratto supposta inizialmente unitaria
% da modificare per soddisfare il requisito sulla forma del modo #2
L1 = 1.;
L2 = 1.;
L3 = 1.;
disp(sprintf('lunghezze travi: L1=%f L2=%f L3=%f', L1, L2, L3));

% rigidezza flessionale posta unitaria
% da dimensionare per ottenere la frequenza sperimentale
EJ = 1;
disp(sprintf('rigidezza flessionale: EJ=%e', EJ));

% rigidezze equivalenti delle travi
k1 = 2*12*EJ/L1^3;
k2 = 2*12*EJ/L2^3;
k3 = 2*12*EJ/L3^3;

% matrice di massa
M = [m, 0, 0;
     0, m, 0;
     0, 0, m];

% matrice di rigidezza
K = [k1 + k2,     -k2,  0;
         -k2, k2 + k3, -k3;
           0,     -k3,  k3];

% calcolo di autovalori e autovettori
[v, lambda2] = eig(M\K);
lambda2 = diag(lambda2);

disp(sprintf('\nrisultati:'))

% frequenze caratteristiche
f = sqrt(lambda2)/2/pi;
for j = 1:length(f),
	disp(sprintf('frequenza #%d: %8.2f Hz modo={% 8.2e,% 8.2e,% 8.2e}', ...
		j, f(j), v(1, j), v(2, j), v(3, j)));
end

