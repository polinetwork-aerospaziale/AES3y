function dy = sistema_non_lineare(t,y)

% Questa funzione consente di risolvere l'equazione differenziale non
% lineare che descrive il moto in grande del sistema in esame

global g
global m1 m3 L R
global k r
global C omega
global v1 v2 v3 om1 om2 lm0 h1 h3 dl0


thetap = y(1);
theta = y(2);

[v1,v2,v3,om1,om2,lm,h1,h3] = cinematica(theta);

[M,dM] = energia_cinetica(v1,v2,v3,om1,om2,theta);

[dlm,ddlm,dh1,dh3,ddh1,ddh3] = energia_potenziale(theta);
dV = k*(lm-lm0+dl0)*dlm+m1*g*dh1+m3*g*dh3;
dD = r*dlm^2*thetap;
Q=(2*C*L*sin(theta)/R)*cos(omega*t);

% Sistema in forma di stato
dy = [-(dM/2*thetap^2+dD+dV-Q)/M;
      thetap];
