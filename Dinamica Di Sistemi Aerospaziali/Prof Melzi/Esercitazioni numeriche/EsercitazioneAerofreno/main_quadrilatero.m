clear
close all
clc

%%%%%%LEGGE DI MOTO DA FILE ESTERNO%%%%%%

load('Legge_di_moto_quadr.mat');                        %Si importa la legge di moto imposta all'asta OA
                                                        %(spostamento,
                                                        %velocit�, accelerazione e il vettore tempo)
                                                        
%Grafici della legge di moto
figure
plot(time,rad2deg(al),'b','Linewidth',2);
xlabel('tempo [s]','interpreter','latex');
ylabel('$\mathrm{\alpha}$ [deg]','interpreter','latex');
title('Spostamento');
set(gca,'FontSize',14);
grid on
box on

figure
plot(time,rad2deg(al_p),'b','Linewidth',2);
xlabel('tempo [s]','interpreter','latex');
ylabel('$\mathrm{\dot{\alpha}~[deg~s^{-1}]}$','interpreter','latex');
title('Velocit�');
set(gca,'FontSize',14);
grid on
box on

figure
plot(time,rad2deg(al_pp),'b','Linewidth',2);
xlabel('tempo [s]','interpreter','latex');
ylabel('$\mathrm{\ddot{\alpha}~[deg~s^{-2}]}$','interpreter','latex');
title('Accelerazione');
set(gca,'FontSize',14);
grid on
box on

%% 

%%%%%%VARIABILI GLOBALI%%%%%%
                                                        
global a b c d de0 al0                                  %Dichiarazione variabili globali: lunghezza delle aste e angoli noti.

%%%%%%%DATI%%%%%%%

g = 9.81;                                               %Accelerazione di gravit�

a = 0.5;                                                %Lunghezza dell'asta OA
b = 0.8;                                                %Lunghezza dell'asta AB
c = 1;                                                  %Lunghezza dell'asta CB
d = 0.8;                                                %Lunghezza dell'asta OC
de = 0;                                                 %Angolo delta costante

m = 2;                                                  %Massa dell'asta CB
J = m*c^2/12;                                           %Inerzia dell'asta CB

%% 

%%%%%%DETERMINAZIONE DELLA POSIZIONE INIZIALE DEL MECCANISMO%%%%%%

al0 = al(1);                                            %La funzione "quadrilatero.m" � usata in fsolve per determinare la configurazione iniziale del meccanismo
de0 = de;                                               %Il valore di beta e gamma all'istante t=0 � determinato a partire da alpha=pi/3 e delta=0
                                                        %Le equazioni di chiusura sono richiamate come handle function all'interno di "quadrilatero.m"
x0 = fsolve(@quadrilatero,[0;pi/3]);

%% 
%%%%%%DISEGNO DELLA CONFIGURAZIONE INIZIALE%%%%%%

alfa=al(1);
beta=x0(1,1);
gamma=x0(2,1);
delta=de;

v1 = [0,a*cos(alfa);0,a*sin(alfa)];
v2 = [a*cos(alfa),a*cos(alfa)+b*cos(beta);...
    a*sin(alfa),a*sin(alfa)+b*sin(beta)];
v4 = [0,d*cos(delta);0,d*sin(delta)];
v3 = [d*cos(delta),d*cos(delta)+c*cos(gamma);...
    d*sin(delta),d*sin(delta)+c*sin(gamma)];

figure
hold on; grid on; box on;
plot(v1(1,:),v1(2,:),'bo-','Linewidth',2);
plot(v2(1,:),v2(2,:),'bo-','Linewidth',2);
plot(v4(1,:),v4(2,:),'bo-','Linewidth',2);
plot(v3(1,:),v3(2,:),'bo-','Linewidth',2);
xlim([-0.5 1.5])
ylim([-0.8 1.5])

xlabel('[m]');
ylabel('[m]');
title('Configurazione iniziale');

%% 
%%%%%%RISOLUZIONE DELLA CINEMATICA NEL TEMPO%%%%%%

x = zeros(2,length(time));
for ii = 1:length(time)
    al0 = al(ii);                                        %Alfa pari al valore istantaneo nella legge di moto data                                             
    de0 = de;
    x(:,ii) = fsolve(@quadrilatero,x0);                  %Condizioni iniziali a fsolve consistono nella soluzione [beta; gamma] all'istante precedente
    x0 = x(:,ii);                                           
end

%%
%%%%%%DISEGNO DEL MECCANISMO IN MOVIMENTO%%%%%%

figure
for ii = 1:length(time)
    
    alfa = al(ii);
    beta = x(1,ii);
    gamma = x(2,ii);
    delta = 0; clf
    hold on; grid on; box on
    v1 = [0,a*cos(alfa);0,a*sin(alfa)];
    v2 = [a*cos(alfa),a*cos(alfa)+b*cos(beta);...
        a*sin(alfa),a*sin(alfa)+b*sin(beta)];
    v4 = [0,d*cos(delta);0,d*sin(delta)];
    v3 = [d*cos(delta),d*cos(delta)+c*cos(gamma);...
        d*sin(delta),d*sin(delta)+c*sin(gamma)];
    plot(v1(1,:),v1(2,:),'bo-','Linewidth',2);
    plot(v2(1,:),v2(2,:),'bo-','Linewidth',2);
    plot(v4(1,:),v4(2,:),'bo-','Linewidth',2);
    plot(v3(1,:),v3(2,:),'bo-','Linewidth',2);
    xlim([-0.6 1.5])
    ylim([-0.8 1.5])
    xlabel('[m]');
    ylabel('[m]');
    title('Movimento del meccanismo');
    drawnow
    
end

%%

%%%%%%DETERMINAZIONE DI VELOCITA' E ACCELERAZIONI%%%%%%
vel_mat = zeros(2,length(time));                              %Velocit� e accelerazione preallocate come vettori di zeri
acc_mat = zeros(2,length(time));                              %Ogni vettore ha componente x e y

for ii = 1:length(time)
    
    alfa = al(ii);
    beta = x(1,ii);
    gamma = x(2,ii);
    delta = 0;
    
    MAT1 = [-b*sin(beta) c*sin(gamma);...                     %Scrittura matrici del sistema lineare per le velocit� (vedere testo esercitazione)
        b*cos(beta) -c*cos(gamma)];
    b1 = [a*al_p(ii)*sin(alfa); -a*al_p(ii)*cos(alfa)];
    vel = MAT1\b1;
    vel_mat(:,ii) = vel;
    
    MAT2 = MAT1;                                              %Scrittura matrici del sistema lineare per le velocit� (vedere testo esercitazione)
    b2 = [a*al_pp(ii)*sin(alfa) + a*al_p(ii)^2*cos(alfa)+b*vel(1)^2*cos(beta) - c*vel(2)^2*cos(gamma);...
        -a*al_pp(ii)*cos(alfa) + a*al_p(ii)^2*sin(alfa)+b*vel(1)^2*sin(beta) - c*vel(2)^2*sin(gamma)];
    acc = MAT2\b2;
    acc_mat(:,ii) = acc;

end

%Teorema di Rivals

vG(1,:) = -vel_mat(2,:)*c/2.*sin(x(2,:));
vG(2,:) = +vel_mat(2,:)*c/2.*cos(x(2,:));

Cm = (m*g*vG(2,:) + (J+m*c^2/4)*vel_mat(2,:).*acc_mat(2,:))./al_p;

%%

%%%%%%GRAFICO DELLA COPPIA NECESSARIA%%%%%%

figure
plot(time,Cm,'b','Linewidth',2);
xlabel('tempo [s]');
ylabel('C_m [Nm]');
title('Coppia alla manovella vs tempo');
grid on;
box on;

figure
plot(rad2deg(al),Cm,'b','Linewidth',2);
xlabel('\alpha [deg]');
ylabel('C_m [Nm]');
title('Coppia alla manovella vs angolo');
grid on;
box on;
