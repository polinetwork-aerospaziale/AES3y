function D_X=Rotazione_Matrice(D_x,alfa)
n = sin(alfa*pi/180);
m = cos(alfa*pi/180);
T =[m*m 	n*n	  2*m*n,
    n*n 	m*m      -2*m*n,
    -m*n        m*n	 (m*m-n*n)];
D_X=inv(T)*D_x*inv(T)';

function eps_alfa=Rotazione_Deformazioni(eps,alfa)
n = sin(angle_rot*pi/180);
m = cos(angle_rot*pi/180);
T =[m*m 	n*n	  2*m*n,
    n*n 	m*m  -2*m*n,
   -m*n   m*n	 (m*m-n*n)];
R =[1 0 0,
    0 1 0,
    0 0 2 ];
eps=inv(R)*eps;
eps_alfa=T*eps;
eps_alfa=R*epsalpha;

function sig_alfa=Rotazione_Sforzi(sig,alfa)
n = sin(angle_rot*pi/180);
m = cos(angle_rot*pi/180);
T =[m*m 	n*n	  m*n,
    n*n 	m*m  m*n,
   -m*n   m*n	 (m*m-n*n)];
R =[1 0 0,
    0 1 0,
    0 0 2 ];
eps=inv(R)*eps;
eps_alfa=T*eps;
eps_alfa=R*epsalpha;
