%=============================================================
% CLT_MAKE : Calcola la matrice elastica di un laminato
%            applicando la Teoria Classica della laminazione
%=============================================================
%
%=============================================================
% STRUTTURA DATI INPUT
%=============================================================
%----------------------------------------------------
% Numero di lamine: L.N
%----------------------------------------------------
L.N=input('Numero lamine nel laminato : ');
%------------------------------------------------------------------------
% Files Caratteristiche Elastiche e Spessori (Tipologia delle lamine):
% Array di stringhe L.Lamine
%------------------------------------------------------------------------
L.Lamine='';
%------------------------------------------------------------------------
% Angoli di Orientamento : Array L.Angoli
%------------------------------------------------------------------------
L.Alfa=zeros(L.N,1);
%=============================================================
% STRUTTURA DATI OUTPUT
%=============================================================
%-----------------------------------------------------------------------
% Matrici di Rigidezza delle lamine
% Array di Matrici 3 x 3
%-----------------------------------------------------------------------
L.Q_lamina=zeros(3,3,L.N);
%------------------------------------------------------------------------
% Posizioni z rispetto alla supeficie media geometrica del laminato
%------------------------------------------------------------------------
L.z=zeros(L.N+1,1);
%------------------------------------------------------------------------
% Spessore del laminato
%------------------------------------------------------------------------
L.TH=0;
%------------------------------------------------------------------------
% Matrici del Laminato
%------------------------------------------------------------------------
% Sottomatrice A
L.A=zeros(3,3);
% Sottomatrice B
L.B=zeros(3,3);
% Sottomatrice D
L.D=zeros(3,3);
%=============================================================
% DISPLAY STRUTTURA DATI
%=============================================================
display(L);

%=============================================================
% INPUT
%=============================================================
for lamina=1:L.N
    %------------------------------------------------------------------------
    % Files Caratteristiche Elastiche e Spessori (Tipologia delle lamine):
    % Array di stringhe L.Lamine
    %------------------------------------------------------------------------
    messaggio=['File Caratteristiche Lamina ',num2str(lamina),' = '];
    file_lamina=input(messaggio,'s');
    L.Lamine=strvcat(L.Lamine,file_lamina);
    %------------------------------------------------------------------------
    % Angoli di Orientamento : Array L.Angoli
    %------------------------------------------------------------------------
    messaggio=['Angolo di Orientamento Lamina ',num2str(lamina),' = '];
    L.Alfa(lamina)=input(messaggio);
end

display(L.Lamine);
display(L.Alfa);

%=============================================================
% ELABORAZIONI DATI DELLE SINGOLE LAMINE
%=============================================================
for lamina=1:L.N
    %------------------------------------------------------------------------
    % Matrice di Flessibilita'
    %------------------------------------------------------------------------
    dati_lamina=load(L.Lamine(lamina,:),'-ASCII');
    Exx=dati_lamina(1);
    Eyy=dati_lamina(2);
    vyx=dati_lamina(3);
    Gxy=dati_lamina(4);
    F_lamina=[ 1/Exx -vyx/Exx 0; -vyx/Exx 1/Eyy 0; 0 0 1/Gxy];
    %------------------------------------------------------------------------
    % Matrice di Rigidezza
    %------------------------------------------------------------------------
    L.Q_lamina(:,:,lamina)=inv(F_lamina);
    %------------------------------------------------------------------------
    % Quote e Spessori
    %------------------------------------------------------------------------
    L.z(lamina+1)=L.z(lamina)+dati_lamina(5);
    L.TH=L.TH+dati_lamina(5);
end
%------------------------------------------------------------------------
% Quote rispetto alla superficie media geometrica
%------------------------------------------------------------------------
L.z=L.z-L.TH/2;

display(L.Q_lamina);
display(L.z);

%=============================================================
% ASSEMBLAGGIO MATRICE DI RIGIDEZZA DEL LAMINATO
%=============================================================
for lamina=1:L.N
    Q_lamina=Rotazione_Matrice(L.Q_lamina(:,:,lamina),L.Alfa(lamina));
    L.A=L.A+Q_lamina*(L.z(lamina+1)-L.z(lamina));
    L.B=L.B+(1/2)*Q_lamina*(L.z(lamina+1)^2-L.z(lamina)^2);
    L.D=L.D+(1/3)*Q_lamina*(L.z(lamina+1)^3-L.z(lamina)^3);
end
display([L.A L.B; L.B L.D]);
L.Q_Laminato=[L.A L.B; L.B L.D];
L.F_Laminato=inv(L.Q_Laminato);

%=============================================================
% MEMORIZZAZIONE SU FILE DELLA MATRICE DEL LAMINATO
%=============================================================
L.file=input('Nome file di dati laminato (senza estensione) : ','s');
save(L.file,'L');
