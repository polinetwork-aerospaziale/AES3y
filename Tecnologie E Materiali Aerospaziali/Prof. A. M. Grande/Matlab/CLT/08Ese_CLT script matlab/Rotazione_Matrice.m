function D_X=Rotazione_Matrice(D_x,alfa)
n = sin(alfa*pi/180);
m = cos(alfa*pi/180);
J =[m*m 	n*n	  2*m*n,
    n*n 	m*m      -2*m*n,
    -m*n        m*n	 (m*m-n*n)];
D_X=inv(J)*D_x*inv(J)';

