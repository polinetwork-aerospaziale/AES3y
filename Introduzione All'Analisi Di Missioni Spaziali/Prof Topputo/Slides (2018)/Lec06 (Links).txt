
Conic section in polar coordinates: http://ptolemeos.lr.tudelft.nl/astrodynamics/conic_section_intro.html

Velocity components: http://ptolemeos.lr.tudelft.nl/astrodynamics/conic_section_velocity_fullscreen.html

Velocity components (explained): http://ptolemeos.lr.tudelft.nl/astrodynamics/conic_section_velocity.html

Orbit in 3D: http://ptolemeos.lr.tudelft.nl/astrodynamics/orbit_integrator_3d.html