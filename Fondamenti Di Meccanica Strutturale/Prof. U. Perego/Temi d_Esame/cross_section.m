%% GEOMETRY PARAMETERS b_i, h_i, y_g_i, shape: "r"=rectangle, "t"=triangle

% geom = {42 6 3 "r";
%         6 42 27 "r";
%         18 6 51 "r"}
% N = 3528;
% T_y = -2880; %[N]
% M_x = 1411200 %[N*mm]
% t = 6 %[mm] (thickness)

geom = {22 7 3.5 "r";
     12 36 25 "r";
     48 6 46 "r";
     24 5 (54-5*2/3) "t";
     24 5 (54-5*2/3) "t"}
 
N = 0;
T_y = 2580; %[N]
M_x = 1960800 %[N*mm]
t = 12 %[mm] (thickness)
 
b = cell2mat(geom(:,1));
h = cell2mat(geom(:,2));
y = cell2mat(geom(:,3));
s = string(geom(:,4));

n = length(b);
A_i_vect = [];
J_x_vect = [];

%% CALCULATE CG AND TOTAL AREA
for i = 1:n
   
    if s(i) == "r"
        A_i = b(i)*h(i);
    elseif s(i) == "t"
        A_i = .5*b(i)*h(i);
    end
    
    A_i_vect = [A_i_vect; A_i];    
end
S = sum(A_i_vect.*y);
A = sum(A_i_vect)
y_g = S/A


%% CALCULATE MOMENT OF INERTIA
for i = 1:n
   
    if s(i) == "r"
        c = 1/12;
    elseif s(i) == "t"
        c = 1/36;
    end
    
    
    J_x_i = c*b(i)*(h(i))^3 + A_i_vect(i)*(y(i)-y_g)^2;

    J_x_vect = [J_x_vect; J_x_i];
    
end
J_x = sum(J_x_vect)

sigma = @(y) N/A + (M_x/J_x).*y;

y_n = - (J_x/M_x)*(N/A)

    
[gBot, iBot] = max(y);
    if s(iBot) == "r"
        h_tot = gBot + h(iBot)/2
    elseif s(iBot) == "t"
        h_tot = gBot + (2/3)*h(iBot)
    end
    
    yBot = h_tot - y_g;
if (y_g+y_n) > yBot %note: y_n with sign
    y_sup = - y_g;
else
    y_sup = yBot
end
    
sigma_max = sigma(y_sup)


% s_star = -20.95;
% sigma_star = sigma(s_star) %[MPa]
% 
% S_x = @(s) 6*s.^2 - 287.4*s-4227.3;
% S_x_star = S_x(s_star);
% tau_star = abs(T_y*S_x_star/(J_x*t)) %[MPa]
% 
% sigma_vM_star = sqrt(sigma_star^2+3*tau_star^2)


