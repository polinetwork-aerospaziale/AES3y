clear;
close all;
clc;

% Data input
v_inf = 1;                    % intensità corrente uniforme

% Impostazioni ovale di Rankine
h = 0.6;
l = 1.2;

% Dimensioni finestra calcolo
x_dim_sym = 5; % i.e. [-5,5]
y_dim_sym = 3; % i.e. [-3,3]

% calcolo di b e lambda per avere le l ed h desiderate
% bisogna risolvere un sistema NON LINEARE.
%b_lambda = @(x) [  (l^2/x(1)^2 - (1+x(2)/(pi*v_inf*x(1))))  ; ( h/x(1)*tan( (h/x(1)) / (x(2)/(pi*v_inf*x(1))) ) -1)];
b_lambda = @(x) [  (l^2/x(1)^2 - (1+x(2)/(pi*v_inf*x(1))))  ; ( v_inf*h-x(2)/2/pi* atan2((2*x(1)*h),(h^2-x(1)^2)))];
inc = fsolve(b_lambda, [l, 5]);

b = inc(1);
lambda = inc(2);


% stream function in coordinate cartesiane
psi_xy  =   @(x, y,  b, l) v_inf*y+...
                                 + lambda/(2*pi) .* atan2(y, x+b)+...
                                 - lambda/(2*pi) .* atan2(y, x-b);

psi_impli = @(x, y, b, l, psi_c) psi_xy(x, y,  b, l)-psi_c;

y0v = linspace(-y_dim_sym, y_dim_sym,30);
xell=linspace(-l,l,100);
yellup = sqrt(h^2*(1-xell.^2/l^2));


% nel ciclo calcolo e sovrappongo le streamlines all'immagine della
% geometria

for j = 1:length(y0v)
    x0 = -x_dim_sym;
    y0 = y0v(j);
    psi_c = psi_xy(x0, y0, b, l);
    xv = linspace(-x_dim_sym, x_dim_sym, 100);
    xx=[];
    yy=[];
    for i = 1:length(xv)   
        x0 = xv(i);
        psi_xx = @(y) psi_impli(x0, y, b, l, psi_c);
        if (i == 1)
           guess = -5;
        else
            guess = yy(end);
        end

        [yp, fval, exitflag, output] = fzero(psi_xx, guess);
        
        if(exitflag~=1)
            disp('zero non trovato.');
        end
        
        if((xv(i)/l)^2+(yp/h)^2> 1)
            xx = [xx xv(i)];
            yy = [yy yp];
        end
        
    end
    
    hold on;
    
    % plot di alcuni punti notevoli dell'ovale
     xf1 =  -b;     yf1 = 0;
     xf2 =  b;      yf2 = 0;
     xf3 = l;       yf3 =0;
     xf4 = 0;       yf4 = -h;    
     xf5 =- l;      yf5 = 0;   
     xf6 = 0;       yf6 = h;
    plot(xx, yy,  'r', xf1, yf1, 'Sg', xf2, yf2, 'Sg',  'LineWidth', 1);
    plot(xf3, yf3, '*y', xf4, yf4, '*r', xf5, yf5, '*y', xf6, yf6, '*r', 'LineWidth', 3);
    hold on;
end

nx = 20; ny =10;
xxx = linspace(-x_dim_sym,x_dim_sym,nx);
yyy = linspace(-y_dim_sym/2,y_dim_sym/2,ny);

U = @(x, y, b, m)  v_inf + m/2/pi*((x+b)./((x+b).^2+y.^2) - (x-b) ./((x-b).^2+y.^2));
V = @(x, y, b, m)  m/2/pi*(y./((x+b).^2+y.^2) - y./((x-b).^2+y.^2));
for i = 1:length(xxx)
    for j = 1:length(yyy)
        if xxx(i).^2/l^2 + yyy(j).^2/h^2 >1
          UUU(i,j) = U(xxx(i),yyy(j),b,lambda);
          VVV(i,j) = V(xxx(i),yyy(j),b,lambda);
        else
            UUU(i,j) =0;
            VVV(i,j) = 0;
        end
    end
end


plot(xell,yellup,'b',xell,-yellup,'b')

xxx= repmat(xxx,[ny,1]);
yyy= repmat(yyy,[nx,1])';
figure
plot(xell,yellup,'b',xell,-yellup,'b', xf1, yf1, 'Sg', xf2, yf2, 'Sg')
hold on
scale = 0.5 ;
quiver(xxx', yyy',UUU,VVV,scale); % Plot the vector field
xlabel('x-axis');
ylabel('y-axis');
axis equal